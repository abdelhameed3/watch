/*global $ */
/*Table Content
==========================
    # Nice Scroll
    # Change image in HTML
    # Loadmore
*/
/* Nice Scroll
===============================*/
$(document).ready(function () {
    
    "use strict";
    
	$("html").niceScroll({
        scrollspeed: 60,
        mousescrollstep: 35,
        cursorwidth: 5,
        cursorcolor: '#000000',
        cursorborder: 'none',
        background: 'rgba(255,255,255,0.3)',
        cursorborderradius: 3,
        autohidemode: false,
        cursoropacitymin: 0.1,
        cursoropacitymax: 1,
        zindex: "999",
        horizrailenabled: false
	});
});
/* Change image in HTML*/
$(document).ready(function () {
    
    "use strict";
   $(".product-item.small").click(function () {
        var v = $(this).html();
        $(".product-item.heig").html(v);
    });
});
/* Loadmore*/
$(document).ready(function () {
    
    "use strict";
    $(".box-hidden").slice(0, 3).show();
    $("#loadmore").on("click", function(e){
        e.preventDefault();

        $(".box-hidden:hidden").slice(0,2).slideDown();
        if($(".box-hidden:hidden").length == 0){
            $(this).fadeOut("slow");
        }
        $("html, body").animate({
            scrollTop: $(this).offset().top - 150
        },500);
    })
});
            
